#! /bin/bash

if [ -e $TMP_DIR ]; then
    echo "'$TMP_DIR' should not already exist"
    exit
fi

for image in $@; do
    d=$TMP_DIR/`dirname $image`    
    f="$TMP_DIR/${image%%.inr.gz}.png"
    mkdir -p $d
    inr2png $image $f
done

$VIEWER $TMP_DIR/* $TMP_DIR/**/* && rm -rf $TMP_DIR
