# disable implicit rules
.SUFFIXES:

INCLUDE_DIR := src
SOURCE_DIR := src
BUILD_ROOT := build
INRIMAGE_DIR ?= ../inrimage_install

COMPILER ?= clang
CC := $(COMPILER)
CFLAGS := -Wextra -Wall -pedantic -std=c11

IMAGE_DIR := images
DEMO_DIR := demos
TMP_DIR := tmp
VIEWER ?= viewnior

ifeq ($(RELEASE), yes)
	CFLAGS += -O3
	BUILD_DIR := $(BUILD_ROOT)/release
	LOG_LEVEL ?= 1
else
	CFLAGS += -g3 -fstack-protector-all -Wshadow -Wunreachable-code \
		  -Wstack-protector -pedantic-errors -O0 -Wundef \
		  -Wfatal-errors -Wstrict-prototypes -Wmissing-prototypes \
		  -Wwrite-strings -Wunknown-pragmas -Wstrict-aliasing \
		  -Wold-style-definition -Wmissing-field-initializers \
		  -Wfloat-equal -Wpointer-arith -Wnested-externs \
		  -Wstrict-overflow=5 -Wswitch-default -Wswitch-enum \
		  -Wbad-function-cast -Wredundant-decls \
		  -fno-omit-frame-pointer -Winline -fstrict-aliasing
	BUILD_DIR := $(BUILD_ROOT)/debug
	LOG_LEVEL ?= 3
endif

CFLAGS += -DLOG_LEVEL=$(LOG_LEVEL)
CFLAGS += -I$(INRIMAGE_DIR)/include/ -I$(INCLUDE_DIR)
LFLAGS := -L$(INRIMAGE_DIR)/lib/ -linrimage -lm

COMMON_SOURCES := $(shell find $(SOURCE_DIR)/common -name *.c)
COMMON_OBJECTS := $(COMMON_SOURCES:$(SOURCE_DIR)/%.c=$(BUILD_DIR)/%.o)

all: source_build_dirs $(BUILD_DIR)/bin/sobel \
                       $(BUILD_DIR)/bin/draw_lines \
                       $(BUILD_DIR)/bin/draw_circles \
                       $(BUILD_DIR)/first_filters/averaging \
                       $(BUILD_DIR)/first_filters/median \
                       $(BUILD_DIR)/harris_detection/harris \
                       $(BUILD_DIR)/hough_detection/lines \
                       $(BUILD_DIR)/hough_detection/circles \
                       $(BUILD_DIR)/segmentation/split \
                       $(BUILD_DIR)/segmentation/merge

MKBIN = @echo "Building $@" && $(CC) $(CFLAGS) $^ -o $@ $(LFLAGS)

$(BUILD_DIR)/bin/sobel: $(SOURCE_DIR)/bin/sobel.c $(COMMON_OBJECTS)
	$(MKBIN)
$(BUILD_DIR)/bin/draw_lines: $(SOURCE_DIR)/bin/draw_lines.c $(COMMON_OBJECTS)
	$(MKBIN)
$(BUILD_DIR)/bin/draw_circles: $(SOURCE_DIR)/bin/draw_circles.c $(COMMON_OBJECTS)
	$(MKBIN)
$(BUILD_DIR)/first_filters/averaging: $(SOURCE_DIR)/first_filters/averaging.c $(COMMON_OBJECTS)
	$(MKBIN)
$(BUILD_DIR)/first_filters/median: $(SOURCE_DIR)/first_filters/median.c $(COMMON_OBJECTS)
	$(MKBIN)
$(BUILD_DIR)/harris_detection/harris: $(SOURCE_DIR)/harris_detection/harris.c $(COMMON_OBJECTS)
	$(MKBIN)
$(BUILD_DIR)/hough_detection/lines: $(SOURCE_DIR)/hough_detection/lines.c $(COMMON_OBJECTS)
	$(MKBIN)
$(BUILD_DIR)/hough_detection/circles: $(SOURCE_DIR)/hough_detection/circles.c $(COMMON_OBJECTS)
	$(MKBIN)

SEGMENTATION_SOURCES := $(COMMON_SOURCES) $(SOURCE_DIR)/segmentation/segmentation.c
SEGMENTATION_OBJECTS := $(SEGMENTATION_SOURCES:$(SOURCE_DIR)/%.c=$(BUILD_DIR)/%.o)

$(BUILD_DIR)/segmentation/split: $(SOURCE_DIR)/segmentation/split.c $(SEGMENTATION_OBJECTS)
	$(MKBIN)
$(BUILD_DIR)/segmentation/merge: $(SOURCE_DIR)/segmentation/merge.c $(SEGMENTATION_OBJECTS)
	$(MKBIN)

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.c $(INCLUDE_DIR)/%.h
	@echo "Compiling $*"
	@$(CC) -c $(CFLAGS) $< -o $@

SOURCE_DIRS := $(shell find $(SOURCE_DIR) -type d)
SOURCE_BUILD_DIRS := $(SOURCE_DIRS:$(SOURCE_DIR)%=$(BUILD_DIR)%)
source_build_dirs:
	mkdir -p $(SOURCE_BUILD_DIRS)

PATH := $(INRIMAGE_DIR)/bin/:$(PATH)
export PATH
export BUILD_DIR
export IMAGE_DIR
export DEMO_DIR
export TMP_DIR
export VIEWER

.PHONY: demos
demos: all
	@echo "Demonstrating first filters"
	@$(SOURCE_DIR)/first_filters/demonstrate.sh
	@echo "Demonstrating harris detection"
	@$(SOURCE_DIR)/harris_detection/demonstrate.sh
	@echo "Demonstrating hough detection"
	@$(SOURCE_DIR)/hough_detection/demonstrate.sh
	@echo "Demonstrating segmentation"
	@$(SOURCE_DIR)/segmentation/demonstrate.sh

.PHONY: view
view:
	@./view.sh $(FILES)

.PHONY: edges
edges:
	@./edges.sh $(FILE)

.PHONY: clean
clean:
	rm -rf $(BUILD_ROOT) $(DEMO_DIR)
