#! /bin/bash

mkdir -p $DEMO_DIR/hough_detection

function circle () {
    $BUILD_DIR/hough_detection/circles $IMAGE_DIR/$1.inr.gz $DEMO_DIR/hough_detection/$1_circle_$2_$3_$4_$5_$6.inr.gz -dxo $2 -dyo $3 -dr $4 -epsilon $5 -threshold $6
}

function line () {
    p=$DEMO_DIR/hough_detection/$1_line_$2_$3_$4_$5
    $BUILD_DIR/hough_detection/lines $IMAGE_DIR/$1.inr.gz $p.inr.gz -dtheta $2 -drho $3 -epsilon $4 -threshold $5
}

circle circle_edges 1 1 1 1 5
circle circle_edges 0.6 0.6 1 0.2 6
line rectangle_edges 0.002 1 0.5 15
line rectangle_edges 0.002 0.8 0.05 10
