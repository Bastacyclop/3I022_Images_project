#include "common/core.h"
#include "common/mem.h"
#include "common/inrimagination.h"
#include "common/math.h"
#include "common/log.h"

static
uint32_t* hough(const float* edges, size_t w, size_t h,
                float d_theta, float d_rho, float epsilon, uint32_t threshold,
                size_t* theta_count, size_t* rho_count,
                float** out_buffer);

static
bool line_belongs(size_t c, size_t l,
                  float theta, float rho,
                  float epsilon);

static
void draw_lines(float* buffer, size_t w, size_t h,
                const uint32_t* accumulator, size_t theta_count, size_t rho_count,
                float theta_min, float rho_min, float d_theta, float d_rho, uint32_t threshold);

static
void hough_draw_line(float* buffer, size_t w, size_t h, float theta, float rho);

static
bool is_local_maximum(const uint32_t* buffer, size_t w, size_t h,
                      uint32_t threshold, size_t p_c, size_t p_l);

uint32_t* hough(const float* edges, size_t w, size_t h,
                float d_theta, float d_rho, float epsilon, uint32_t threshold,
                size_t* theta_count, size_t* rho_count,
                float** out_buffer) {
    const float theta_min = -PI_F32 / 2;
    const float theta_max = PI_F32 / 2;
    const float rho_min = 0.0;
    const float rho_max = sqrt(pow((float)w, 2.0) + pow((float)h, 2.0));

    *theta_count = (size_t)((theta_max - theta_min) / d_theta);
    *rho_count = (size_t)((rho_max - rho_min) / d_rho);
    uint32_t* accumulator = mem_alloc_zeroed((*theta_count) * (*rho_count) * sizeof(uint32_t));

    for (size_t r = 0; r < *rho_count; r++) {
        float rho = (float)r*d_rho + rho_min;

        for (size_t t = 0; t < *theta_count; t++) {
            float theta = (float)t*d_theta + theta_min;

            for (size_t l = 0; l < h; l++) {
                for (size_t c = 0; c < w; c++) {
                    size_t p = l*w + c;

                    if (edges[p] >= 0.5) {
                        if (line_belongs(c, l, theta, rho, epsilon)) {
                            accumulator[r*(*theta_count) + t]++;
                        }
                    }
                }
            }
        }
    }

    *out_buffer = mem_alloc_zeroed(w * h * sizeof(float));
    draw_lines(*out_buffer, w, h,
               accumulator, *theta_count, *rho_count,
               theta_min, rho_min, d_theta, d_rho, threshold);

    return accumulator;
}

bool line_belongs(size_t c, size_t l,
                  float theta, float rho,
                  float epsilon) {
    return fabs((cos(theta) * (float)c) + (sin(theta) * (float)l) - rho) < epsilon;
}

void draw_lines(float* buffer, size_t w, size_t h,
                const uint32_t* accumulator, size_t theta_count, size_t rho_count,
                float theta_min, float rho_min, float d_theta, float d_rho, uint32_t threshold) {
    for (size_t r = 0; r < rho_count; r++) {
        float rho = (float)r*d_rho + rho_min;

        for (size_t t = 0; t < theta_count; t++) {
            float theta = (float)t*d_theta + theta_min;

            if (is_local_maximum(accumulator, theta_count, rho_count, threshold, t, r)) {
                hough_draw_line(buffer, w, h, theta, rho);
            }
        }
    }
}

void hough_draw_line(float* buffer, size_t w, size_t h, float theta, float rho) {
    // left: (x = c = 0) <-> (y = l = ρ / sinθ)
    // bottom: (y = l = 0) <-> (x = c = ρ / cosθ)
    // right: (x = c = w - 1) <-> (y = l = (ρ - (w - 1)*cosθ) / sinθ)
    // top: (y = l = h - 1) <-> (x = c = (ρ - (h - 1)*sinθ) / cosθ)
    size_t left_c = 0;
    size_t bottom_l = 0;
    size_t right_c = w - 1;
    size_t top_l = h - 1;
    float left_x = (float)(left_c);
    float bottom_y = (float)(bottom_l);
    float right_x = (float)(right_c);
    float top_y = (float)(top_l);

    #define valid_x(x) (left_x <= x && x <= right_x)
    #define valid_y(y) (bottom_y <= y && y <= top_y)
    size_t i = 0;
    size_t intersections[2][2];

    #define intersect(prefix) \
        intersections[i][0] = prefix##_c; \
        intersections[i][1] = prefix##_l; \
        i++

    float c = cos(theta);
    float s = sin(theta);

    if (s != 0.0) {
        float left_y = rho / s;
        if (valid_y(left_y)) {
            size_t left_l = (size_t)(left_y);
            intersect(left);
        }

        float right_y = (rho - right_x * c) / s;
        if (valid_y(right_y)) {
            size_t right_l = (size_t)(right_y);
            intersect(right);
        }
    }

    if (c != 0.0) {
        if (i < 2) {
            float bottom_x = rho / c;
            if (valid_x(bottom_x)) {
                size_t bottom_c = (size_t)(bottom_x);
                intersect(bottom);
            }

            if (i < 2) {
                float top_x = (rho - top_y * s) / c;
                if (valid_x(top_x)) {
                    size_t top_c = (size_t)(top_x);
                    intersect(top);
                }
            }
        }
    }

    if (i != 2) {
        fatal("hough draw_line didn't find two intersections");
    }

    draw_line(buffer, w, h,
              intersections[0][0], intersections[0][1],
              intersections[1][0], intersections[1][1]);

    #undef valid_x
    #undef valid_y
    #undef intersect
}

bool is_local_maximum(const uint32_t* buffer, size_t w, size_t h,
                      uint32_t threshold, size_t p_c, size_t p_l) {
    uint32_t v = buffer[p_l*w + p_c];

    for (int32_t d_l = -1; d_l <= 1; d_l++) {
        for (int32_t d_c = -1; d_c <= 1; d_c++) {
            if (d_l == 0 && d_c == 0) continue;

            int32_t l = (int32_t)p_l + d_l;
            int32_t c = (int32_t)p_c + d_c;

            if (0 <= l && l < (int32_t)h &&
                0 <= c && c < (int32_t)w) {
                if (v <= threshold ||
                    v <= buffer[l*w + c]) {
                    return false;
                }
            }
        }
    }

    return true;
}


const char* usage = "[input] [output] -dtheta dt -drho dr -epsilon e -threshold t";

int main(int argc, char** argv) {
    inrimaginate(argc, argv, usage);

    float d_theta;
    get_value_option("-dtheta", "%f", &d_theta);

    float d_rho;
    get_value_option("-drho", "%f", &d_rho);

    float epsilon;
    get_value_option("-epsilon", "%f", &epsilon);

    uint32_t threshold;
    get_value_option("-threshold", "%d", &threshold);


    char path[256];
    get_input_file_option(path);

    ImageFormat format;
    Image* image = image_open(path, &format);
    assert_greyscale(&format);

    size_t w = format.dim_x;
    size_t h = format.dim_y;

    float* edges = mem_alloc(w * h * sizeof(float));
    

    image_read_lines_as_float(image, h, edges);
    image_close(image);

    size_t theta_count;
    size_t rho_count;
    float* out_buffer;
    uint32_t* accumulator = hough(edges, w, h,
                                  d_theta, d_rho, epsilon, threshold,
                                  &theta_count, &rho_count,
                                  &out_buffer);

    get_output_file_option(path);
    image = image_create(path, &format);
    image_write_lines_as_float(image, h, out_buffer);
    image_close(image);
    
    mem_free(edges);
    mem_free(accumulator);
    mem_free(out_buffer);
    return EXIT_SUCCESS;
}
