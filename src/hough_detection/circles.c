#include "common/core.h"
#include "common/mem.h"
#include "common/inrimagination.h"
#include "common/math.h"
#include "common/log.h"

static
float* hough(const float* edges, size_t w, size_t h,
             float d_xo, float d_yo, float d_r, float epsilon,
             uint32_t);

static
bool approx_in_circle(size_t x, size_t y,
                      float xo, float yo, float r,
                      float epsilon);

static
void draw_circles(float* buffer, size_t w, size_t h,
                  const uint32_t* accumulator, size_t n_xo, size_t n_yo, size_t n_r,
                  float xo_min, float yo_min, float r_min,
                  float d_xo, float d_yo, float d_r,
                  uint32_t threshold);

static
bool is_local_maximum(const uint32_t* accumulator, size_t n_xo, size_t n_yo, size_t n_r,
                      size_t i_xo, size_t i_yo, size_t i_r,
                      uint32_t threshold);

static
size_t accumulator_index(size_t n_xo, size_t n_yo, size_t n_r,
                         size_t xo, size_t yo, size_t r);

float* hough(const float* edges, size_t w, size_t h,
             float d_xo, float d_yo, float d_r, float epsilon,
             uint32_t threshold) {
    const float xo_min = 0.0;
    const float xo_max = (float)(w - 1);
    const float xo_paf = xo_max - xo_min;
    const float yo_min = 0.0;
    const float yo_max = (float)(h - 1);
    const float yo_paf = yo_max - yo_min;
    const float r_min = 0.0;
    const float r_max = sqrt((xo_paf * xo_paf) + (yo_paf * yo_paf)) / 2.;

    const size_t n_xo = (size_t)(xo_paf / d_xo);
    const size_t n_yo = (size_t)(yo_paf / d_yo);
    const size_t n_r = (size_t)((r_max - r_min) / d_r);
    uint32_t* accumulator = mem_alloc_zeroed(n_xo * n_yo * n_r * sizeof(uint32_t));

    for (size_t i_xo = 0; i_xo < n_xo; i_xo++) {
        float xo = (float)i_xo*d_xo + xo_min;

        for (size_t i_yo = 0; i_yo < n_yo; i_yo++) {
            float yo = (float)i_yo*d_yo + yo_min;

            for (size_t i_r = 0; i_r < n_r; i_r++) {
                float r = (float)i_r*d_r + r_min;

                for (size_t y = 0; y < h; y++) {
                    for (size_t x = 0; x < w; x++) {
                        size_t p = y*w + x;

                        if (edges[p] >= 0.5) {
                            if (approx_in_circle(x, y, xo, yo, r, epsilon)) {
                                accumulator[accumulator_index(
                                    n_xo, n_yo, n_r, i_xo, i_yo, i_r
                                )]++;
                            }
                        }
                    }
                }
            }
        }
    }

    float* out_buffer = mem_alloc_zeroed(w * h * sizeof(float));
    draw_circles(out_buffer, w, h,
                 accumulator, n_xo, n_yo, n_r,
                 xo_min, yo_min, r_min,
                 d_xo, d_yo, d_r,
                 threshold);

    mem_free(accumulator);
    return out_buffer;
}

size_t accumulator_index(size_t n_xo, size_t n_yo, size_t n_r,
                         size_t xo, size_t yo, size_t r) {
    (void) n_xo;
    return (xo*n_yo + yo)*n_r + r;
}

bool approx_in_circle(size_t x, size_t y,
                      float xo, float yo, float r,
                      float epsilon) {
    float x_paf = (float)x - xo;
    float y_paf = (float)y - yo;
    return fabs((x_paf * x_paf) + (y_paf * y_paf) - (r * r)) < epsilon;
}

void draw_circles(float* buffer, size_t w, size_t h,
                  const uint32_t* accumulator, size_t n_xo, size_t n_yo, size_t n_r,
                  float xo_min, float yo_min, float r_min,
                  float d_xo, float d_yo, float d_r,
                  uint32_t threshold) {
    for (size_t i_xo = 0; i_xo < n_xo; i_xo++) {
        float xo = (float)i_xo*d_xo + xo_min;

        for (size_t i_yo = 0; i_yo < n_yo; i_yo++) {
            float yo = (float)i_yo*d_yo + yo_min;

            for (size_t i_r = 0; i_r < n_r; i_r++) {
                float r = (float)i_r*d_r + r_min;

                if (is_local_maximum(accumulator, n_xo, n_yo, n_r,
                                     i_xo, i_yo, i_r, threshold)) {
                    draw_circle(buffer, w, h, xo, yo, r);
                }
            }
        }
    }
}

bool is_local_maximum(const uint32_t* accumulator, size_t n_xo, size_t n_yo, size_t n_r,
                      size_t i_xo, size_t i_yo, size_t i_r,
                      uint32_t threshold) {
    uint32_t v = accumulator[accumulator_index(
        n_xo, n_yo, n_r, i_xo, i_yo, i_r
    )];

    if (v <= threshold) return false;

    for (int32_t d_xo = -1; d_xo <= 1; d_xo++) {
        for (int32_t d_yo = -1; d_yo <= 1; d_yo++) {
            for (int32_t d_r = -1; d_r <= 1; d_r++) {
                if (d_xo == 0 && d_yo == 0 && d_r == 0) continue;

                int32_t xo = (int32_t)i_xo + d_xo;
                int32_t yo = (int32_t)i_yo + d_yo;
                int32_t r = (int32_t)i_r + d_r;

                if (0 <= xo && xo < (int32_t)n_xo &&
                    0 <= yo && yo < (int32_t)n_yo &&
                    0 <= r && r < (int32_t)n_r) {
                    if (v <= accumulator[accumulator_index(
                                n_xo, n_yo, n_r, xo, yo, r
                            )]) {
                        return false;
                    }
                }
            }
        }
    }

    return true;
}


const char* usage = "[input] [output] -dxo d -dyo d -dr d -epsilon e -threshold";

int main(int argc, char** argv) {
    inrimaginate(argc, argv, usage);

    float d_xo;
    get_value_option("-dxo", "%f", &d_xo);

    float d_yo;
    get_value_option("-dyo", "%f", &d_yo);

    float d_r;
    get_value_option("-dr", "%f", &d_r);

    float epsilon;
    get_value_option("-epsilon", "%f", &epsilon);

    uint32_t threshold;
    get_value_option("-threshold", "%d", &threshold);

    char path[256];
    get_input_file_option(path);

    ImageFormat format;
    Image* image = image_open(path, &format);
    assert_greyscale(&format);

    size_t w = format.dim_x;
    size_t h = format.dim_y;

    float* edges = mem_alloc(w * h * sizeof(float));

    image_read_lines_as_float(image, h, edges);
    image_close(image);

    float* out_buffer = hough(edges, w, h,
                              d_xo, d_yo, d_r, epsilon,
                              threshold);

    get_output_file_option(path);
    image = image_create(path, &format);
    image_write_lines_as_float(image, h, out_buffer);
    image_close(image);

    mem_free(edges);
    mem_free(out_buffer);
    return EXIT_SUCCESS;
}
