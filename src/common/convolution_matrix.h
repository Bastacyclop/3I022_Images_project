#ifndef CONVOLUTION_MATRIX_H
#define CONVOLUTION_MATRIX_H

#include "core.h"

void convolution(const float* kernel, size_t size,
                 const float* buffer, size_t w, size_t h,
                 float* out_buffer);

void init_averaging_filter(float* kernel, size_t size);
void init_gaussian_filter(float* kernel, size_t size, float sigma);

void sobel_derive_x(const float* buffer, size_t w, size_t h,
                    float* out_buffer);
void sobel_derive_y(const float* buffer, size_t w, size_t h,
                    float* out_buffer);
void sobel(const float* buffer, size_t w, size_t h,
           float* out_buffer);

#endif // CONVOLUTION_MATRIX_H
