#ifndef MY_OPTION_H
#define MY_OPTION_H

#include <stdbool.h>

#define Option(T) Option_##T

#define def_option(T) \
    typedef struct { \
        bool is_some; \
        T value; \
    } Option(T);

#define None { .is_some = false }
#define Some(v) { .is_some = true, .value = v }

#endif // MY_OPTION_H
