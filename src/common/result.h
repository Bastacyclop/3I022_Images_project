#ifndef MY_RESULT_H
#define MY_RESULT_H

#include <stdbool.h>

#define Result(T, E) Result_##T##_##E

#define def_result(T, E) \
    typedef struct { \
        bool is_ok; \
        union { \
            T value; \
            E error; \
        }; \
    } Result(T, E);

#define Ok(V) { .is_ok = true, .value = V }
#define Err(E) { .is_ok = false, .error = E }

#endif // MY_RESULT_H
