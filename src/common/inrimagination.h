#ifndef INRIMAGINATION_H
#define INRIMAGINATION_H

#include "core.h"

void inrimaginate(int argc, char** argv, const char* usage);
void get_value_option(const char* option, const char* format, void* v);
void get_input_file_option(char* path);
void get_output_file_option(char* path);

typedef struct {
    uint32_t dim_x;
    uint32_t dim_y;
    uint32_t bsize;
    uint32_t type;
    uint32_t ndim_x;
    uint32_t ndim_y;
    uint32_t ndim_v;
    uint32_t ndim_z;
    uint32_t exp;
} ImageFormat;

const uint32_t IMAGE_TYPE_FIXED;
const uint32_t IMAGE_TYPE_PACKED;
const uint32_t IMAGE_TYPE_FLOAT;

typedef struct image Image;

Image* image_open(const char* path, ImageFormat* fmt);
Image* image_create(const char* path, const ImageFormat* fmt);
void image_close(Image* i);

void image_read_lines(Image* i, int count, void* buffer);
void image_write_lines(Image* i, int count, const void* buffer);
void image_seek_line(Image* i, int line);

void image_read_lines_as_float(Image* i, int count, float* buffer);
void image_write_lines_as_float(Image* i, int count, const float* buffer);

void assert_greyscale(const ImageFormat* format);


void local_maximums(const float* buffer, size_t w, size_t h,
                    float threshold, float* out_buffer);

void draw_cross(const float* buffer, size_t w, size_t h,
                size_t m_l, size_t m_c, int32_t radius,
                float* out_buffer);

void draw_line(float* buffer, size_t w, size_t h,
               int32_t beg_x, int32_t beg_y,
               int32_t end_x, int32_t end_y);

void draw_circle(float* buffer, size_t w, size_t h,
                 int32_t center_x, int32_t center_y, int32_t radius);

#endif // INRIMAGINATION_H
