#include "inrimagination.h"
#include "common/math.h"
#include "common/log.h"
#include "inrimage/image.h"

void inrimaginate(int argc, char** argv, const char* usage) {
    inr_init(argc, argv, (char*)"0.00", (char*)usage, (char*)"");
}

void get_value_option(const char* option, const char* format, void* v) {
    igetopt1((char*)option, (char*)format, v);
}

void get_input_file_option(char* path) {
    infileopt(path);
}

void get_output_file_option(char* path) {
    outfileopt(path);
}

const uint32_t IMAGE_TYPE_FIXED = FIXE;
const uint32_t IMAGE_TYPE_PACKED = PACKEE;
const uint32_t IMAGE_TYPE_FLOAT = REELLE;

static
Image* image_open_or_create(const char* path, ImageFormat* fmt, bool create);

Image* image_open_or_create(const char* path, ImageFormat* fmt, bool create) {
    char* mode = (char*)(create ? "c" : "e");
    return image_((char*)path, mode, (char*)"", (Fort_int*)fmt);
}

Image* image_open(const char* path, ImageFormat* fmt) {
    return image_open_or_create(path, fmt, false);
}

Image* image_create(const char* path, const ImageFormat* fmt) {
    return image_open_or_create(path, (ImageFormat*)fmt, true);
}

void image_close(Image* i) {
    fermnf_(&i);
}

void image_read_lines(Image* i, int count, void* buffer) {
    c_lect(i, count, buffer);
}

void image_write_lines(Image* i, int count, const void* buffer) {
    c_ecr(i, count, (void*)buffer);
}

void image_seek_line(Image* i, int line) {
    c_lptset(i, line);
}

void image_read_lines_as_float(Image* i, int count, float* buffer) {
    c_lecflt(i, count, buffer);
}

void image_write_lines_as_float(Image* i, int count, const float* buffer) {
    c_ecrflt(i, count, (float*)buffer);
}


void assert_greyscale(const ImageFormat* format) {
    if (format->ndim_z != 1 ||
        format->ndim_v != 1) {

        fatal("not a greyscale image format");
    }
}

static
bool is_local_maximum(const float* buffer, size_t w, size_t h,
                      float threshold, size_t p_l, size_t p_c);

void local_maximums(const float* buffer, size_t w, size_t h,
                    float threshold, float* out_buffer) {
    for (size_t l = 0; l < h; l++) {
        for (size_t c = 0; c < w; c++) {
            float* out = &out_buffer[l*w + c];

            if (is_local_maximum(buffer, w, h, threshold, l, c)) {
                *out = 1.0;
            } else {
                *out = 0.0;
            }
        }
    }
}

bool is_local_maximum(const float* buffer, size_t w, size_t h,
                      float threshold, size_t p_l, size_t p_c) {
    float v = buffer[p_l*w + p_c];

    for (int32_t d_l = -1; d_l <= 1; d_l++) {
        for (int32_t d_c = -1; d_c <= 1; d_c++) {
            if (d_l == 0 && d_c == 0) continue;

            int32_t l = (int32_t)p_l + d_l;
            int32_t c = (int32_t)p_c + d_c;

            if (0 <= l && l < (int32_t)h &&
                0 <= c && c < (int32_t)w) {
                if (v <= threshold ||
                    v <= buffer[l*w + c]) {
                    return false;
                }
            }
        }
    }

    return true;
}

void draw_cross(const float* buffer, size_t w, size_t h,
                size_t m_l, size_t m_c, int32_t radius,
                float* out_buffer) {

    float color = 0.0;
    size_t count = 0;

    for (int32_t d_l = -(radius + 2); d_l <= (radius + 2); d_l++) {
        for (int32_t d_c = -(radius + 2); d_c <= (radius + 2); d_c++) {

            int32_t l = (int32_t)m_l + d_l;
            int32_t c = (int32_t)m_c + d_c;

            if (0 <= l && l < (int32_t)h &&
                0 <= c && c < (int32_t)w) {

                size_t i = (size_t)l*w + m_c;
                color += buffer[i];
                count++;
            }
        }
    }
    color = color / (float)count;
    color = fmod(color + 0.5, 1.0);

    for (int32_t d_l = -radius; d_l <= radius; d_l++) {
        if (d_l == 0) continue;

        int32_t l = (int32_t)m_l + d_l;
        if (0 <= l && l < (int32_t)h) {
            size_t i = (size_t)l*w + m_c;
            out_buffer[i] = color;
        }
    }

    for (int32_t d_c = -radius; d_c <= radius; d_c++) {
        int32_t c = (int32_t)m_c + d_c;
        if (0 <= c && c < (int32_t)w) {
            size_t i = m_l*w + (size_t)c;
            out_buffer[i] = color;
        }
    }
}

void draw_line(float* buffer, size_t w, size_t h,
               int32_t beg_x, int32_t beg_y,
               int32_t end_x, int32_t end_y) {
    // Bresenham algorithm
    int32_t dx = abs(end_x - beg_x);
    int32_t dy = -abs(end_y - beg_y);
    int32_t sx = (beg_x <= end_x) ? 1 : -1;
    int32_t sy = (beg_y <= end_y) ? 1 : -1;
    int32_t error = dx + dy;

    int32_t x = beg_x;
    int32_t y = beg_y;
    
    while (true) {
        if (0 <= x && x < (int32_t)w &&
            0 <= y && y < (int32_t)h) {
            buffer[y*w + x] = 1.0;
        }

        int32_t e2 = error * 2;
        if (e2 >= dy) {
            if (x == end_x) break;
            error += dy;
            x += sx;
        }
        if (e2 <= dx) {
            if (y == end_y) break;
            error += dx;
            y += sy;
        }
    }
}

void draw_circle(float* buffer, size_t w, size_t h,
                 int32_t center_x, int32_t center_y, int32_t radius) {
    // Midpoint circle algorithm (close to Bresenham's)
    int32_t x = radius;
    int32_t y = 0;
    int32_t decision = 1 - x;

    #define draw_octant(x, y) { \
        int32_t _x = x + center_x; \
        int32_t _y = y + center_y; \
        if (0 <= _x && _x < (int32_t)w && \
            0 <= _y && _y < (int32_t)h) { \
            buffer[_y*w + _x] = 1.0; \
        } \
    }

    while (y <= x) {
        draw_octant( x,  y);
        draw_octant( y,  x);
        draw_octant(-x,  y);
        draw_octant(-y,  x);
        draw_octant(-x, -y);
        draw_octant(-y, -x);
        draw_octant( x, -y);
        draw_octant( y, -x);
        y++;

        if (decision <= 0) {
            decision += 2 * y + 1;
        } else {
            x--;
            decision += 2 * (y - x) + 1;
        }
    }

    #undef draw_octant
}
