#include "convolution_matrix.h"
#include "common/mem.h"
#include "common/math.h"

static
void pixel_convolution(const float* kernel, int32_t radius,
                       const float* buffer, int32_t w, int32_t h,
                       float* out_buffer, int32_t p_l, int32_t p_c);

void pixel_convolution(const float* kernel, int32_t radius,
                       const float* buffer, int32_t w, int32_t h,
                       float* out_buffer, int32_t p_l, int32_t p_c) {
    float value = 0;
    int32_t size = 2*radius + 1;

    for (int32_t kernel_l = 0; kernel_l < size; kernel_l++) {
        for (int32_t kernel_c = 0; kernel_c < size; kernel_c++) {
            int32_t l = p_l + radius - kernel_l;
            int32_t c = p_c + radius - kernel_c;

            if (l < 0) {
                l = 0;
            } else if (l >= h) {
                l = h - 1;
            }

            if (c < 0) {
                c = 0;
            } else if (c >= w) {
                c = w - 1;
            }

            value += kernel[kernel_l*size + kernel_c] *
                     buffer[l*w + c];
        }
    }

    out_buffer[p_l*w + p_c] = value;
}

void convolution(const float* kernel, size_t size,
                 const float* buffer, size_t w, size_t h,
                 float* out_buffer) {
    assert(size % 2 == 1);
    int32_t radius = size / 2;

    for (size_t l = 0; l < h; l++) {
        for (size_t c = 0; c < w; c++) {
            pixel_convolution(kernel, radius, buffer, (int32_t)w, (int32_t)h, out_buffer, l, c);
        }
    }
}

void init_averaging_filter(float* kernel, size_t size) {
    assert(size % 2 == 1);
    size_t size_sq = size*size;
    float ratio = 1. / (float)(size_sq);

    for (size_t k = 0; k < size_sq; k++) {
        kernel[k] = ratio;
    }
}

void init_gaussian_filter(float* kernel, size_t size, float sigma) {
    assert(size % 2 == 1);
    int32_t radius = (int32_t)size / 2;
    float s = 2. * sigma * sigma;

    for (size_t i = 0; i < size; i++) {
        int32_t rel_i = (int32_t)i - radius;
        int32_t rel_i_sq = rel_i * rel_i;

        for (size_t j = 0; j < size; j++) {
            int32_t rel_j = (int32_t)j - radius;
            int32_t rel_j_sq = rel_j * rel_j;

            float u = 1. / (PI_F32 * s);
            float v = (rel_i_sq + rel_j_sq) / s;
            kernel[i*size + j] = u * exp(-v);
        }
    }
}


void sobel_derive_x(const float* buffer, size_t w, size_t h,
                    float* out_buffer) {
    float kernel[] = {
        -1., 0., 1.,
        -2., 0., 2.,
        -1., 0., 1.
    };

    convolution(kernel, 3, buffer, w, h, out_buffer);
}

void sobel_derive_y(const float* buffer, size_t w, size_t h,
                    float* out_buffer) {
    float kernel[] = {
        -1., -2., -1.,
         0.,  0.,  0.,
         1.,  2.,  1.
    };

    convolution(kernel, 3, buffer, w, h, out_buffer);
}

void sobel(const float* buffer, size_t w, size_t h,
           float* out_buffer) {
    float* dx = out_buffer; // should be fine overlapping those
    sobel_derive_x(buffer, w, h, dx);
    float* dy = mem_alloc(w * h * sizeof(float));
    sobel_derive_y(buffer, w, h, dy);

    for (size_t l = 0; l < h; l++) {
        for (size_t c = 0; c < w; c++) {
            size_t i = l*w + c;

            float dx_sq = dx[i] * dx[i];
            float dy_sq = dy[i] * dy[i];
            out_buffer[i] = sqrt(dx_sq + dy_sq);
        }
    }

    mem_free(dy);
}
