#! /bin/bash

mkdir -p $DEMO_DIR/first_filters

function averaging() {
    $BUILD_DIR/first_filters/averaging $IMAGE_DIR/$1.inr.gz $DEMO_DIR/first_filters/$1_averaging_$2.inr.gz -d $2
}

function median() {
   $BUILD_DIR/first_filters/median $IMAGE_DIR/$1.inr.gz $DEMO_DIR/first_filters/$1_median_$2.inr.gz -n $2
}

averaging cameraman 3
averaging cameraman 5
averaging cameraman 9
averaging lena 3
averaging lena 5
averaging lena 9
averaging window 9

median cameraman 3
median cameraman 9
median cameraman 21
median lena 3
median lena 9
median lena 21
median window 9
