#include "common/core.h"
#include "common/mem.h"
#include "common/inrimagination.h"

static
int compare_floats(const void* a, const void* b);

static
void handle_pixel(const float* buffer, int32_t w, int32_t h,
                  float* out_buffer, int32_t p_l, int32_t p_c,
                  int32_t radius, float* median_buffer);

static
void median_filter(size_t size,
                   const float* buffer, size_t w, size_t h,
                   float* out_buffer);

int compare_floats(const void* pa, const void* pb) {
    float a = *(const float*)pa;
    float b = *(const float*)pb;
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
}

void handle_pixel(const float* buffer, int32_t w, int32_t h,
                  float* out_buffer, int32_t p_l, int32_t p_c,
                  int32_t radius, float* median_buffer) {
    size_t length = 0;

    for (int32_t rel_l = -radius; rel_l <= radius; rel_l++) {
        for (int32_t rel_c = -radius; rel_c <= radius; rel_c++) {
            int32_t l = p_l + rel_l;
            int32_t c = p_c + rel_c;

            if (l >= 0 && l < h &&
                c >= 0 && c < w) {

                median_buffer[length] = buffer[l*w + c];
                length++;
            }

        }
    }

    qsort(median_buffer, length, sizeof(float), compare_floats);
    out_buffer[p_l*w + p_c] = median_buffer[length / 2];
}

void median_filter(size_t size,
                   const float* buffer, size_t w, size_t h,
                   float* out_buffer) {
    assert(size % 2 == 1);
    int32_t radius = size / 2;

    float* median_buffer = mem_alloc(size * size * sizeof(float));

    for (size_t l = 0; l < h; l++) {
        for (size_t c = 0; c < w; c++) {
            handle_pixel(buffer, (int32_t)w, (int32_t)h,
                         out_buffer, l, c,
                         radius, median_buffer);
        }
    }

    mem_free(median_buffer);
}

const char* usage = "[input] [output] -n size";

int main(int argc, char** argv) {
    inrimaginate(argc, argv, usage);

    size_t size;
    get_value_option("-n", "%zu", &size);

    char path[256];
    get_input_file_option(path);

    ImageFormat format;
    Image* image = image_open(path, &format);
    assert_greyscale(&format);

    float* buffer = mem_alloc(format.dim_x * format.dim_y * sizeof(float));

    image_read_lines_as_float(image, format.dim_y, buffer);
    image_close(image);

    float* out_buffer = mem_alloc(format.dim_x * format.dim_y * sizeof(float));

    median_filter(size,
                  buffer, format.dim_x, format.dim_y,
                  out_buffer);

    get_output_file_option(path);
    image = image_create(path, &format);

    image_write_lines_as_float(image, format.dim_y, out_buffer);
    image_close(image);

    mem_free(buffer);
    mem_free(out_buffer);

    return EXIT_SUCCESS;
}
