#include "common/core.h"
#include "common/mem.h"
#include "common/inrimagination.h"
#include "common/convolution_matrix.h"

const char* usage = "[input] [output] -d kernel_size";

int main(int argc, char** argv) {
    inrimaginate(argc, argv, usage);

    size_t size;
    get_value_option("-d", "%zu", &size);

    char path[256];
    get_input_file_option(path);

    ImageFormat format;
    Image* image = image_open(path, &format);
    assert_greyscale(&format);

    float* buffer = mem_alloc(format.dim_x * format.dim_y * sizeof(float));

    image_read_lines_as_float(image, format.dim_y, buffer);
    image_close(image);

    float* out_buffer = mem_alloc(format.dim_x * format.dim_y * sizeof(float));

    float* kernel = mem_alloc(size * size * sizeof(float));

    init_averaging_filter(kernel, size);
    convolution(kernel, size, buffer, format.dim_x, format.dim_y, out_buffer);

    get_output_file_option(path);
    image = image_create(path, &format);

    image_write_lines_as_float(image, format.dim_y, out_buffer);
    image_close(image);

    mem_free(buffer);
    mem_free(out_buffer);
    mem_free(kernel);

    return EXIT_SUCCESS;
}
