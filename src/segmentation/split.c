#include "common/core.h"
#include "common/mem.h"
#include "common/inrimagination.h"

#include "segmentation.h"

const char* usage = "[input] [output] -smallest s -variance v";

int main(int argc, char** argv) {
    inrimaginate(argc, argv, usage);

    size_t smallest;
    get_value_option("-smallest", "%zu", &smallest);

    float variance;
    get_value_option("-variance", "%f", &variance);

    char path[256];
    get_input_file_option(path);

    ImageFormat format;
    Image* image = image_open(path, &format);
    assert_greyscale(&format);

    size_t w = format.dim_x;
    size_t h = format.dim_y;

    float* buffer = mem_alloc(w * h * sizeof(float));

    image_read_lines_as_float(image, h, buffer);
    image_close(image);

    QuadTree tree = split(buffer, w, h, smallest, variance);

    float* out_buffer = mem_alloc(w * h * sizeof(float));
    draw_areas(&tree, out_buffer, w, h);

    get_output_file_option(path);
    image = image_create(path, &format);
    image_write_lines_as_float(image, h, out_buffer);
    image_close(image);

    mem_free(buffer);
    drop_quad_tree(&tree);
    mem_free(out_buffer);
    return EXIT_SUCCESS;
}
