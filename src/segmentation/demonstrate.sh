#! /bin/bash

mkdir -p $DEMO_DIR/segmentation

function split () {
    $BUILD_DIR/segmentation/split $IMAGE_DIR/$1.inr.gz $DEMO_DIR/segmentation/$1_split_$2_$3.inr.gz -smallest $2 -variance $3
}

function merge () {
    $BUILD_DIR/segmentation/merge $IMAGE_DIR/$1.inr.gz $DEMO_DIR/segmentation/$1_merge_$2_$3.inr.gz -smallest $2 -variance $3
}

split lena 1 0.1
split lena 1 0.01
split lena 1 0.001
split lena 1 0.00001

split cameraman 1 0.005
split cameraman 10 0.005

merge lena 1 0.01
merge lena 1 0.00001

merge cameraman 10 0.005
