#include "segmentation.h"

#include "common/core.h"
#include "common/mem.h"
#include "common/math.h"
#include "common/log.h"

static
void split_rec(const float* buffer, size_t w, size_t h,
               size_t smallest_area, float variance_threshold,
               QuadTree* tree);

static
bool homogenous(const Area* a, float variance_threshold);

static
QuadTree make_tree_leaf(const float* buffer, size_t w, size_t h, Area a);

static
void push_area_leafs(const QuadTree* tree, Vec* v);

static
bool has_neighbor(const Merge* m, const Area* a);

static
bool neighbors(const Area* a, const Area* b);

static
bool try_merge(Merge* m, const Area* a, float variance_threshold);



QuadTree split(const float* buffer, size_t w, size_t h,
               size_t smallest_area, float variance_threshold) {
    Area a = (Area) {
        .x = 0,
        .y = 0,
        .w = w,
        .h = h,
    };

    QuadTree root = make_tree_leaf(buffer, w, h, a);
    split_rec(buffer, w, h, smallest_area, variance_threshold, &root);
    return root;
}

void split_rec(const float* buffer, size_t w, size_t h,
               size_t smallest_area, float variance_threshold,
               QuadTree* tree) {
    (void) h;

    const Area* area = &tree->area;
    size_t n_pixels = area->w * area->h;

    if (!homogenous(area, variance_threshold) &&
        n_pixels >= smallest_area) {

        size_t hw = area->w / 2;
        size_t hh = area->h / 2;

        Area br = (Area) {
            .x = area->x + hw,
            .y = area->y,
            .w = area->w - hw,
            .h = hh,
        };

        Area tr = (Area) {
            .x = area->x + hw,
            .y = area->y + hh,
            .w = area->w - hw,
            .h = area->h - hh,
        };

        Area tl = (Area) {
            .x = area->x,
            .y = area->y + hh,
            .w = hw,
            .h = area->h - hh,
        };

        Area bl = (Area) {
            .x = area->x,
            .y = area->y,
            .w = hw,
            .h = hh,
        };

        tree->split = mem_alloc(sizeof(Split));
        *tree->split = (Split) {
            .br = make_tree_leaf(buffer, w, h, br),
            .tr = make_tree_leaf(buffer, w, h, tr),
            .tl = make_tree_leaf(buffer, w, h, tl),
            .bl = make_tree_leaf(buffer, w, h, bl)
        };

        split_rec(buffer, w, h, smallest_area, variance_threshold, &tree->split->br);
        split_rec(buffer, w, h, smallest_area, variance_threshold, &tree->split->tr);
        split_rec(buffer, w, h, smallest_area, variance_threshold, &tree->split->tl);
        split_rec(buffer, w, h, smallest_area, variance_threshold, &tree->split->bl);
    }
}

bool homogenous(const Area* a, float variance_threshold) {
    return a->variance < variance_threshold;
}

QuadTree make_tree_leaf(const float* buffer, size_t w, size_t h, Area a) {
    (void) h;

    float sum = 0.0;
    float square_sum = 0.0;
    for (size_t y = a.y; y < (a.y + a.h); y++) {
        for (size_t x = a.x; x < (a.x + a.w); x++) {
            float v = buffer[y*w + x];
            sum += v;
            square_sum += v * v;
        }
    }

    float n_pixels = (float)(a.w * a.h);
    a.average = sum / n_pixels;
    a.variance = (square_sum / n_pixels) - (a.average * a.average);

    return (QuadTree) {
        .area = a,
        .split = NULL
    };
}

void draw_areas(const QuadTree* tree, float* buffer, size_t w, size_t h) {
    if (tree->split) {
        draw_areas(&tree->split->br, buffer, w, h);
        draw_areas(&tree->split->tr, buffer, w, h);
        draw_areas(&tree->split->tl, buffer, w, h);
        draw_areas(&tree->split->bl, buffer, w, h);
    } else {
        const Area* a = &tree->area;

        for (size_t y = a->y; y < (a->y + a->h); y++) {
            for (size_t x = a->x; x < (a->x + a->w); x++) {
                buffer[y*w + x] = a->average;
            }
        }
    }
}

void drop_quad_tree(QuadTree* tree) {
    if (tree->split) {
        drop_quad_tree(&tree->split->br);
        drop_quad_tree(&tree->split->tr);
        drop_quad_tree(&tree->split->tl);
        drop_quad_tree(&tree->split->bl);
        mem_free(tree->split);
    }
}



Vec merge(const QuadTree* tree, float variance_threshold) {
    Vec remaining_leafs = Vec_new(sizeof(const Area*));
    push_area_leafs(tree, &remaining_leafs);
    Vec merges = Vec_new(sizeof(Merge));

    const Area* a;
    while (Vec_pop(&remaining_leafs, &a)) {
        Merge m = (Merge) {
            .areas = Vec_new(sizeof(const Area*)),
            .average = a->average,
            .variance = a->variance
        };
        Vec_push(&m.areas, &a);

        size_t i = 0;
        while (i < Vec_len(&remaining_leafs)) {
            const Area* b = *(const Area**)Vec_get(&remaining_leafs, i);

            if (has_neighbor(&m, b) && try_merge(&m, b, variance_threshold)) {
                Vec_swap_remove(&remaining_leafs, i, NULL);
                i = 0;
            } else {
                i++;
            }
        }

        Vec_push(&merges, &m);
    }

    Vec_drop(&remaining_leafs);
    return merges;
}

void push_area_leafs(const QuadTree* tree, Vec* v) {
    if (tree->split) {
        push_area_leafs(&tree->split->br, v);
        push_area_leafs(&tree->split->tr, v);
        push_area_leafs(&tree->split->tl, v);
        push_area_leafs(&tree->split->bl, v);
    } else {
        const Area* a = &tree->area;
        Vec_push(v, &a);
    }
}

bool has_neighbor(const Merge* m, const Area* a) {
    for (size_t i = 0; i < Vec_len(&m->areas); i++) {
        const Area* b = *(const Area**)Vec_get(&m->areas, i);

        if (neighbors(a, b)) {
            return true;
        }
    }

    return false;
}

bool neighbors(const Area* a, const Area* b) {
    #define corners(area) \
        size_t area##_right = area->x + area->w; \
        size_t area##_top = area->y + area->h; \
        size_t area##_left = area->x; \
        size_t area##_bottom = area->y

    corners(a);
    corners(b);

    #undef corners

    if (a_left == b_right || a_right == b_left) {
        return a_bottom < b_top && b_bottom < a_top;
    } else if (a_bottom == b_top || a_top == b_bottom) {
        return a_left < b_right && b_left < a_right;
    }
    return false;
}

bool try_merge(Merge* m, const Area* a, float variance_threshold) {
    float a_n_pixels = (float)(a->w * a->h);
    float n_pixels = m->n_pixels + a_n_pixels;

    float average = (m->average * m->n_pixels +
                     a->average * a_n_pixels) / n_pixels;

    float m_square_sum_average = m->variance + (m->average * m->average);
    float a_square_sum_average = a->variance + (a->average * a->average);
    float square_sum_average = (m_square_sum_average * m->n_pixels +
                                a_square_sum_average * a_n_pixels) / n_pixels;

    float variance = square_sum_average - (average * average);
    if (variance < variance_threshold) {
        m->n_pixels = n_pixels;
        m->average = average;
        m->variance = variance;
        Vec_push(&m->areas, &a);
        return true;
    } else {
        return false;
    }
}

void draw_merges(const Vec* merges, float* buffer, size_t w, size_t h) {
    (void) h;

    for (size_t m = 0; m < Vec_len(merges); m++) {
        const Merge* merge = (const Merge*)Vec_get(merges, m);

        for (size_t a = 0; a < Vec_len(&merge->areas); a++) {
            const Area* area = *(const Area**)Vec_get(&merge->areas, a);

            for (size_t y = area->y; y < (area->y + area->h); y++) {
                for (size_t x = area->x; x < (area->x + area->w); x++) {
                    buffer[y*w + x] = merge->average;
                }
            }
        }
    }
}

void drop_merge(Merge* m) {
    Vec_drop(&m->areas);
}
