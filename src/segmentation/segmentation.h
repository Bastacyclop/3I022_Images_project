#ifndef SEGMENTATION_H
#define SEGMENTATION_H

#include "common/vec.h"

typedef struct {
    size_t x, y;
    size_t w, h;
    float average;
    float variance;
} Area;

typedef struct Split Split;
typedef struct {
    Area area;
    Split* split;
} QuadTree;

struct Split {
    QuadTree br;
    QuadTree tr;
    QuadTree tl;
    QuadTree bl;
};

QuadTree split(const float* buffer, size_t w, size_t h,
               size_t smallest_area, float variance_threshold);

void draw_areas(const QuadTree* tree, float* buffer, size_t w, size_t h);

void drop_quad_tree(QuadTree* tree);

typedef struct {
    Vec areas;
    float n_pixels;
    float average;
    float variance;
} Merge;

Vec merge(const QuadTree* tree, float variance_threshold);

void draw_merges(const Vec* merges, float* buffer, size_t w, size_t h);

void drop_merge(Merge* m);

#endif // SEGMENTATION_H
