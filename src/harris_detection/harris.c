#include "common/core.h"
#include "common/mem.h"
#include "common/inrimagination.h"
#include "common/convolution_matrix.h"
#include "common/math.h"

static
void harris(const float* buffer, size_t w, size_t h,
            float sigma, float kappa, float threshold,
            float* out_buffer);

static
void multiply_images(const float* buffer_a, const float* buffer_b,
                     size_t w, size_t h,
                     float* out_buffer);


void harris(const float* buffer, size_t w, size_t h,
            float sigma, float kappa, float threshold,
            float* out_buffer) {
    #define allocate_image(identifier) \
        float* identifier = mem_alloc(w * h * sizeof(float));

    float* dx = out_buffer;
    sobel_derive_x(buffer, w, h, dx);
    allocate_image(dy);
    sobel_derive_y(buffer, w, h, dy);

    size_t gaussian_size = 2 * ceil(3 * sigma) + 1;
    float* gaussian_kernel = mem_alloc(gaussian_size * gaussian_size * sizeof(float));
    init_gaussian_filter(gaussian_kernel, gaussian_size, sigma);

    allocate_image(dxx);
    multiply_images(dx, dx, w, h, dxx);
    allocate_image(dxy);
    multiply_images(dx, dy, w, h, dxy);
    float* dyy = dx;
    multiply_images(dy, dy, w, h, dyy);

    float* a11 = dy;
    convolution(gaussian_kernel, gaussian_size, 
                dxx, w, h, a11);
    float* a12 = dxx;
    convolution(gaussian_kernel, gaussian_size, 
                dxy, w, h, a12);
    float* a22 = dxy;
    convolution(gaussian_kernel, gaussian_size, 
                dyy, w, h, a22);

    mem_free(gaussian_kernel);
    //out_buffer = dyy;
    float* harris_detection = a11;
    float* maximums = a12;
    
    for (size_t l = 0; l < h; l++) {
        for (size_t c = 0; c < w; c++) {
            size_t i = l*w + c;

            float det = a11[i] * a22[i] - a12[i] * a12[i];
            float trace = a11[i] + a22[i];
            harris_detection[i] = det - kappa * trace * trace;
        }
    }

    local_maximums(harris_detection, w, h, threshold, maximums);

    mem_copy(buffer, out_buffer, w * h * sizeof(float)); 

    for (size_t l = 0; l < h; l++) {
        for (size_t c = 0; c < w; c++) {
            size_t i = l*w + c;

            if (maximums[i] >= 0.5) {
                draw_cross(buffer, w, h, l, c, 2, out_buffer);
            }
        }
    }

    mem_free(a22);
    mem_free(harris_detection);
    mem_free(maximums);

    #undef allocate_image
}

void multiply_images(const float* buffer_a, const float* buffer_b,
                     size_t w, size_t h,
                     float* out_buffer) {
    for (size_t l = 0; l < h; l++) {
        for (size_t c = 0; c < w; c++) {
            size_t i = l*w + c;
            out_buffer[i] = buffer_a[i] * buffer_b[i];
        }
    }
}

const char* usage = "[input] [output] -sigma s -kappa k -threshold t";

int main(int argc, char** argv) {
    inrimaginate(argc, argv, usage);

    float sigma;
    get_value_option("-sigma", "%f", &sigma);

    float kappa;
    get_value_option("-kappa", "%f", &kappa);

    float threshold;
    get_value_option("-threshold", "%f", &threshold);

    char path[256];
    get_input_file_option(path);

    ImageFormat format;
    Image* image = image_open(path, &format);
    assert_greyscale(&format);

    size_t w = format.dim_x;
    size_t h = format.dim_y;

    float* buffer = mem_alloc(w * h * sizeof(float));

    image_read_lines_as_float(image, h, buffer);
    image_close(image);

    float* out_buffer = mem_alloc(w * h * sizeof(float));

    harris(buffer, w, h, sigma, kappa, threshold, out_buffer);

    get_output_file_option(path);
    image = image_create(path, &format);

    image_write_lines_as_float(image, h, out_buffer);
    image_close(image);

    mem_free(buffer);
    mem_free(out_buffer);

    return EXIT_SUCCESS;
}
