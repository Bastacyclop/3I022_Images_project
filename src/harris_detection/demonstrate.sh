#! /bin/bash

mkdir -p $DEMO_DIR/harris_detection

function harris() {
    $BUILD_DIR/harris_detection/harris $IMAGE_DIR/$1.inr.gz $DEMO_DIR/harris_detection/$1_$2_$3_$4.inr.gz -sigma $2 -kappa $3 -threshold $4
}

harris lena 2 0.02 0
harris lena 1 0.02 0.1

harris cameraman 2 0.001 0
harris cameraman 1 0.02 0.1

harris window 2 0.02 0
harris window 5 0.02 0
