#include "common/core.h"
#include "common/mem.h"
#include "common/inrimagination.h"

const char* usage = "[output]";

int main(int argc, char** argv) {
    inrimaginate(argc, argv, usage);

    char path[256];
    get_output_file_option(path);

    size_t w = 100;
    size_t h = 100;

    ImageFormat format = (ImageFormat) {
        .dim_x = w,
        .dim_y = h,
        .ndim_x = w,
        .ndim_y = h,
        .ndim_z = 1,
        .ndim_v = 1,
        .type = IMAGE_TYPE_FIXED,
        .bsize = 1,
        .exp = 200
    };

    Image* image = image_create(path, &format);
    float* buffer = mem_alloc_zeroed(w * h * sizeof(float));

    #define circle(cx, cy, r) \
        draw_circle(buffer, w, h, cx, cy, r)
    circle(-25, -25, 75);
    circle(25, 25, 10);
    circle(49, 49, 20);
    circle(74, 74, 10);
    circle(125, 125, 75);
    #undef circle

    image_write_lines_as_float(image, h, buffer);
    image_close(image);

    mem_free(buffer);
    return EXIT_SUCCESS;
}
