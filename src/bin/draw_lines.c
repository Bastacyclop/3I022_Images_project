#include "common/core.h"
#include "common/mem.h"
#include "common/inrimagination.h"

const char* usage = "[output]";

int main(int argc, char** argv) {
    inrimaginate(argc, argv, usage);

    char path[256];
    get_output_file_option(path);

    size_t w = 100;
    size_t h = 100;

    ImageFormat format = (ImageFormat) {
        .dim_x = w,
        .dim_y = h,
        .ndim_x = w,
        .ndim_y = h,
        .ndim_z = 1,
        .ndim_v = 1,
        .type = IMAGE_TYPE_FIXED,
        .bsize = 1,
        .exp = 200
    };

    Image* image = image_create(path, &format);
    float* buffer = mem_alloc_zeroed(w * h * sizeof(float));

    #define line(ax, ay, bx, by) \
        draw_line(buffer, w, h, ax, ay, bx, by)
    line(0, 50, 100, 50);
    line(0, 25, 100, 75);
    line(1, 1, 98, 98);
    line(25, 0, 75, 100);
    line(50, 0, 50, 100);
    #undef line

    image_write_lines_as_float(image, h, buffer);
    image_close(image);

    mem_free(buffer);
    return EXIT_SUCCESS;
}
