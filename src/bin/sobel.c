#include "common/core.h"
#include "common/mem.h"
#include "common/inrimagination.h"
#include "common/convolution_matrix.h"

const char* usage = "[input] [output]";

int main(int argc, char** argv) {
    inrimaginate(argc, argv, usage);

    char path[256];
    get_input_file_option(path);

    ImageFormat format;
    Image* image = image_open(path, &format);
    assert_greyscale(&format);

    size_t w = format.dim_x;
    size_t h = format.dim_y;

    float* buffer = mem_alloc(w * h * sizeof(float));

    image_read_lines_as_float(image, h, buffer);
    image_close(image);

    float* out_buffer = mem_alloc(w * h * sizeof(float));

    sobel(buffer, w, h, out_buffer);

    get_output_file_option(path);
    image = image_create(path, &format);

    image_write_lines_as_float(image, h, out_buffer);
    image_close(image);

    mem_free(buffer);
    mem_free(out_buffer);

    return EXIT_SUCCESS;
}
