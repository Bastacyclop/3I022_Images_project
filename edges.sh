#! /bin/bash

out="${1%%.inr.gz}_edges.inr.gz"
detc -sob $1 | mh -n 0 | aff > $out
